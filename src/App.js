
import {useState} from 'react';

import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';

import Error from './pages/Error';

import {
  //BrowserRouter as Router,
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import AppNavBar from './components/AppNavBar';
import Footer from './components/Footer';

import {UserProvider} from './UserContext';

function App() {

  const [user, setUser] = useState({
    id: 'hello',
    isAdmin: true
  });

  return (
    //BrowserRouter
    
    <UserProvider value={{user, setUser}}>
      <BrowserRouter>
        <AppNavBar/>
        <Routes>
          <Route path="/" element={ <Home/> } />
          <Route path="/courses" element={<Courses/>}/>
          <Route path="/register" element={<Register/>}/>
          <Route path="/login" element={<Login/>}/>
          <Route path="*" element={<Error/>}/>
        </Routes>
        <Footer/>
      </BrowserRouter>
    </UserProvider>
    
  )
   
}

export default App;
