import { Navbar, Nav, Container } from 'react-bootstrap';

const email = localStorage.getItem(`email`)

export default function AppNavBar(){
    return(
        <Navbar bg="info" expand="lg">
          <Container>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link className="text-light" href="/">Home</Nav.Link>
                <Nav.Link className="text-light" href="/courses">Course</Nav.Link>
                {
                  email !== null 
                  ? 
                    <Nav.Link className="text-light" href="/logout">Logout</Nav.Link>
                  :
                  <>
                    <Nav.Link className="text-light" href="/register">Sign Up</Nav.Link>
                    <Nav.Link className="text-light" href="/login">Login</Nav.Link>
                  </>
                }
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar> 
    )
}
