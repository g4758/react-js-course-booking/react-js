import { useEffect, useState } from 'react';
import { Card, Button } from 'react-bootstrap';

export default function CourseCard({courseProp}){
    // console.log(courseProp)


    let [count, setCount] = useState(0)
    let [seat, setSeat] = useState(30)


    const {name, description, price} = courseProp
    // console.log(name)
    // console.log(description)
    // console.log(price)

    // useEffect(() => {
    //   console.log('render')
    // }, [count])

    const handleClick = () => {
      // console.log(`test`, count++)
      if(seat > 0){
        setCount(count + 1)
        setSeat(seat - 1)
      }else{
        alert(`No more seats.`)
      }
    }
    return(
        <Card className='m-5'>
            <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>
              {description}
              </Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>{price}</Card.Text>
              <Card.Text>Count: {count}</Card.Text>
              <Card.Text>{count} Enrollees</Card.Text>
              <Button variant="primary" onClick={handleClick}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}
