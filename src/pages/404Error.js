import Banner from "../components/Banner";

export default function Error(){
    let props = {
        h1: <h1 className="display-4"> Error. Page not found</h1>,
        p: <p className="lead">Go back to the <a href="/">homepage</a>.</p>
    }

    return(
        <>
            <Banner bannerProp={ props }/>
        </>
    )
}
