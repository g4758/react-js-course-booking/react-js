import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Form, Button, Row, Col, Container } from "react-bootstrap";

export default function Login(){
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [isDisabled, setIsDisabled] = useState(true)

    const navigate = useNavigate()

    useEffect(() => {
        if(email !== "" && password !== ""){
            setIsDisabled(false)
        }else{
            setIsDisabled(true)
        }

    }, [email, password])

    const loginUser = (e) => {
        e.preventDefault()
        fetch(`https://enigmatic-brushlands-64791.herokuapp.com/api/users/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email, password
            })
        })
        .then(result => result.json())
        .then(result => {
            if(result){
                localStorage.setItem(`token`, result.token)
                let token = localStorage.getItem(`token`)
                fetch(`https://enigmatic-brushlands-64791.herokuapp.com/api/users/profile`, {
                    method:"GET",
                    headers: {
                        "Authorization": `Bearer ${token}`
                    }
                })
                .then(result => result.json())
                .then(result => { 
                    localStorage.setItem(`isAdmin`, result.isAdmin)
                    localStorage.setItem(`id`, result._id)
                    localStorage.setItem(`email`, result.email)
                    alert(`Login successfully`)
                    setEmail("")
                    setPassword("")
                    navigate('/courses')
                })
            }else{
                alert(`Something went wrong. Please try again.`)
            }
        })
    }

    return(
        <Container className="m-5 mx-auto">
           <h1 className="text-center">Login</h1>
           <Row className="justify-content-center">
               <Col xs={12} md={6}>
                    <Form onSubmit={(e) => loginUser(e)}>
                      
                      <Form.Group className="mb-3">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
                      </Form.Group>

                      <Form.Group className="mb-3">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                      </Form.Group>

                      <Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
                    </Form>        
               </Col>
           </Row>
        </Container>
    )
}
