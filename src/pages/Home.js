import Banner from "../components/Banner";
import Highlights from "../components/Highlights";


export default function Home(){

    let props = {
        h1: <h1 className="display-4">Welcome to Course Booking App</h1>,
        p: <p className="lead">Opportunities for everyone, everywhere.</p>,
        btn: <a className='btn btn-info' href='#'>Click Here</a>
    }
    return(
        <>
            <Banner bannerProp={ props }/>
            <Highlights/>
        </>
    )
}
