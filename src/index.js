import React, { Fragment } from 'react';
import { render } from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';


import App from './App';


// ReactDOM.render(what to render, where to render);

render(
  <Fragment>
    <App/>
  </Fragment>
  ,
  document.getElementById(`root`)
);
